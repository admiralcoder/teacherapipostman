#!/bin/bash

# Execute the command and capture the output
output=$(ls -lh)

# Create an HTML report
cat <<EOF >nsreport.html
<!DOCTYPE html>
<html>
<head>
    <title>Directory Listing</title>
</head>
<body>
    <h1>Directory Listing</h1>
    <pre>$output</pre>
</body>
</html>
EOF

# Save the HTML report to a file
# echo "$html_report" > directory_listing.html

# Save the HTML report as an artifact
# cp directory_listing.html /builds/admiralcoder/teacherapipostman
